package ru.tsc.pavlov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.ISaltSettings;
import ru.tsc.pavlov.tm.constant.HashSalt;

public interface HashUtil {

    @Nullable
    static String salt( @NotNull final ISaltSettings setting,
                        @NotNull final String value
    ) {
        if (setting == null) return null;
        @Nullable String secret = setting.getPassSecret();
        @Nullable final Integer iteration = setting.getPassIteration();
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + HashSalt.SECRET);
        }
        return result;
    }

    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @Nullable final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull final java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
