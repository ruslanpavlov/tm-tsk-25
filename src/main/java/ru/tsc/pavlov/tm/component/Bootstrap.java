package ru.tsc.pavlov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.*;
import ru.tsc.pavlov.tm.api.service.*;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyCommandException;
import ru.tsc.pavlov.tm.exception.system.UnknownCommandException;
import ru.tsc.pavlov.tm.repository.*;
import ru.tsc.pavlov.tm.service.*;
import ru.tsc.pavlov.tm.api.service.IPropertyService;
import ru.tsc.pavlov.tm.util.StringUtil;
import ru.tsc.pavlov.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.reflections.Reflections;
import java.lang.reflect.Modifier;
import java.util.Set;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService, propertyService);



    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.pavlov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.tsc.pavlov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            try {
                execute(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    public void start(@Nullable final String[] args) {
        logService.debug("Test environment");
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        initData();
        initCommands();
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Command complete.");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }

    }

    private void initUser(){
        userService.create("ADMIN","Qwerty","admin@yandex.ru");
        userService.create("USER","123456","user@mail.ru");
    }

    @NotNull String adminId = userService.create("ADMIN","Qwerty",UserRole.ADMIN).getId();
    @NotNull String userId = userService.create("USER","123456",UserRole.USER).getId();

    private void initData() {
        projectService.create(userId,"Project №6", "number 7");
        projectService.create(userId,"Project №5", "number 5");
        projectService.create(adminId,"Project №3", "number 3");
        projectService.create(adminId,"Project №2", "number 2");
        projectService.create(adminId,"Project №1", "number 1");
        projectService.create(adminId,"Project №4", "number 4");
    }

    public void parseCommand(@Nullable final String command) {
        if (StringUtil.isEmpty(command)) throw new EmptyCommandException();
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        final UserRole[] roles = abstractCommand.roles();
        authService.checkRoles(roles);

        abstractCommand.execute();
    }

    public void execute(@NotNull final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(@Nullable final String arg) {
        final AbstractCommand command = commandService.getCommandByArg(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (StringUtil.isEmpty(args)) return false;
        else {
            final String arg = args[0];
            parseArg(arg);
            return true;
        }
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IUserService getUserService() { return userService; }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

}
