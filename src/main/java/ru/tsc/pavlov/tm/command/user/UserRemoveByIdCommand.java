package ru.tsc.pavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand{

    @Override
    public UserRole[] roles() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.USER_REMOVE_BY_ID_COMMAND;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User remove by Id";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("PLEASE ENTER USER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final User user = getUserService().removeById(userId,id);
        System.out.println("USER DELETED");
    }

}
