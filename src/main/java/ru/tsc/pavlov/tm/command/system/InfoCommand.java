package ru.tsc.pavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.util.NumberUtil;

public class InfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.INFO;
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display system information";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        @NotNull final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        @NotNull final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
