package ru.tsc.pavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;

public class AboutApplication extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.VERSION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: " + serviceLocator.getPropertyService().getDeveloperName());
        System.out.println("E-MAIL:" + serviceLocator.getPropertyService().getDeveloperEmail());
    }

}
