package ru.tsc.pavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractOwnerEntity extends AbstractEntity{

    @Nullable
    private String userId;

    @Nullable
    private String name;

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@Nullable String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

}
