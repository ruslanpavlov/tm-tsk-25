package ru.tsc.pavlov.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
