package ru.tsc.pavlov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull String getPassSecret();

    @NotNull Integer getPassIteration();

    @NotNull String getApplicationVersion();

    @NotNull String getDeveloperName();

    @NotNull String getDeveloperEmail();

}
