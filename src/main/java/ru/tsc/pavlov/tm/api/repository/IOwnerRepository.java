package ru.tsc.pavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository <E extends AbstractOwnerEntity> extends IRepository<E>  {

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E add(@NotNull String userId, @NotNull final E entity);

    @Nullable
    void remove(@NotNull String userId, @NotNull final E entity);

    @Nullable
    void clear(@NotNull final String userId);

    @Nullable
    boolean existsById(@NotNull final String id);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Integer getSize(@NotNull String userId);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    E findByName(@NotNull String userId, @NotNull String name);

}
