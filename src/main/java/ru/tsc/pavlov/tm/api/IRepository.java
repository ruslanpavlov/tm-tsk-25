package ru.tsc.pavlov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity>{

    void add(final E entity);

    void remove(final E entity);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(Comparator<E> comparator);

    void clear();

    boolean existsById(final String id);

    @NotNull
    E findById(String id);

    @Nullable
    E removeById(String userId, String id);

    @NotNull
    Integer getSize();

}
