package ru.tsc.pavlov.tm.exception.entity;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error: Entity not found.");
    }

}
